//
//  ViewController3.swift
//  Lesson_8
//
//  Created by Vladdoriy on 22.11.2022.
//

import Foundation
import UIKit

class ViewController3: UIViewController {

    @IBOutlet weak var button: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func didPressedBack(_ backButton: UIButton) {
       
        navigationController?.popViewController(animated: true)
    }
}
